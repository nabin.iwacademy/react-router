import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Home from "./components/Home";
import About from "./components/About";
import Navbars from "./components/Navbar";
import Services from "./components/Services";
import Product from "./components/Product";
import "bootstrap/dist/css/bootstrap.min.css";
import PageNotFound from "./components/PageNotFound";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Navbars />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route
            path="/about"
            exact
            render={props => <About {...props} name="nabin" />}
          />
          <Route path="/services" exact component={Services} />
          <Route path="/product/:id" exact component={Product} />
          <Route path="/404" component={PageNotFound}></Route>
          <Redirect to="/404" />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
