import React, { Component } from "react";

class About extends Component {
  handleClick = () => {
    // this.props.history.push("/services");
    this.props.history.replace("/services");
  };
  render() {
    console.log(this.props);
    return (
      <div>
        <h1> About Page</h1>
        <button onClick={this.handleClick}>Login</button>
      </div>
    );
  }
}

export default About;
